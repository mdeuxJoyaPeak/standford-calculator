//
//  CalculatorEngine.swift
//  Stanford calculator
//
//  Created by MacBook Pro on 1/9/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import Foundation


struct AppEngine {
    
    private  enum operation {
        case unary((Double)->Double)
        case binary((Double,Double) -> Double)
        case equal
        case constant(Double)
    }
    private var operationLookUp : Dictionary<String,operation>
        = [
            "P" : operation.constant (Double.pi),
            "e" : operation.constant(M_E),
            "➕": operation.binary({ return $0 + $1 }),
            "➖": operation.binary({ return $0 - $1 }),
            "➗": operation.binary({ return $0 / $1 }),
            "✖️": operation.binary({ return $0 * $1 }),
            "√" :operation.unary(sqrt),
            "=" :operation.equal
    ]
    
    mutating func DoOperation (_ op :String )
    {
        if let op1 = operationLookUp[op]
        {
            switch(op1)
            {
            case .binary(let bCallback ):
                if ( prevOperand != nil){
                    acumulator = bCallback(prevOperand!,acumulator!)
                }
                currOperation = bCallback

            case .constant(let cCallBack):
                acumulator = cCallBack
            case .unary( let uCallBack):
                acumulator = uCallBack(acumulator!)
            case .equal:
                DoPendingOperation()
            }
        }
    }
    
   private  var currOperation : ((Double,Double)-> Double)? = nil
   mutating func DoPendingOperation()
    {
        if ( prevOperand != nil &&  acumulator != nil && currOperation != nil){
            acumulator = currOperation!(prevOperand!,acumulator!)
            
        }
    }
    private var prevOperand : Double?
    mutating func SetOperand ( _ operand: Double)
    {
        if let _prevOperand = acumulator
        {
            prevOperand = _prevOperand
        }
        acumulator = operand
    }
    
    private var acumulator :Double?
    var Result : Double
    {
        get {
            return acumulator!
        }
    }
    mutating func freePrevOperand()
    {
        prevOperand = nil;
    }
    
    
}
