//
//  ViewController.swift
//  Stanford calculator
//
//  Created by MacBook Pro on 1/9/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var displayString: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        displayString.text!.removeAll()
    }
    private var calcAPI = AppEngine()
    
    var display : String
    {
        set {  displayString.text = String( newValue) }
        get {  return  displayString.text! }
    }
    var isTyping = false
    @IBAction func buttonWasTouched(_ sender: UIButton)
    {
        if (isTyping)
        {
            display += sender.currentTitle!
            
        }
        else
        {
            display = sender.currentTitle!
            
        }
        
        isTyping = true
    }
    
    @IBAction func operation(_ sender: UIButton)
    {
        isTyping = false
        if let disVal = Double(display){
        calcAPI.SetOperand(disVal)
        }
        if let curOp = sender.currentTitle
        {
            calcAPI.DoOperation(curOp)
        }
        display = String(calcAPI.Result)
    }
    @IBAction func clearAll(_ sender: UIButton)
    {
        display = String(0);
        calcAPI.freePrevOperand()
    }
    
}

